-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 07-Jun-2018 às 18:55
-- Versão do servidor: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemaslds`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acompanhamento`
--

CREATE TABLE `acompanhamento` (
  `ACOM_ID` int(10) NOT NULL,
  `ACOM_COMENT` text NOT NULL,
  `ACOM_NUMCHA` int(11) NOT NULL,
  `ACOM_USER` varchar(10) NOT NULL,
  `ACOM_USERNOM` varchar(50) DEFAULT NULL,
  `ACOM_DATA` datetime(6) DEFAULT NULL,
  `ACOM_OCOR` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `carga_horaria`
--

CREATE TABLE `carga_horaria` (
  `CH_NMDISC` varchar(50) NOT NULL,
  `CH_NOMUSR` varchar(50) NOT NULL,
  `CH_DATA` date NOT NULL,
  `CH_HRINI` time NOT NULL,
  `CH_HRFIM` time NOT NULL,
  `CH_TOTHR` time NOT NULL,
  `CH_TOTPEN` int(11) NOT NULL,
  `CH_ADDNOT` int(11) NOT NULL,
  `CH_IDDIARIO` int(10) NOT NULL,
  `CH_IDMAT` varchar(11) NOT NULL,
  `CH_CODUSR` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `carga_horaria`
--

INSERT INTO `carga_horaria` (`CH_NMDISC`, `CH_NOMUSR`, `CH_DATA`, `CH_HRINI`, `CH_HRFIM`, `CH_TOTHR`, `CH_TOTPEN`, `CH_ADDNOT`, `CH_IDDIARIO`, `CH_IDMAT`, `CH_CODUSR`) VALUES
('Interação Humano Computador', 'GIOVANNI', '2018-05-09', '05:00:00', '09:00:00', '10:00:00', 10, 2, 1, 'IHCD5', 'PF0000001'),
('', 'GIOVANNI', '2018-05-16', '11:28:00', '07:00:00', '00:00:00', 0, 0, 2, 'IHCD5', 'PF0000001');

--
-- Acionadores `carga_horaria`
--
DELIMITER $$
CREATE TRIGGER `Tgr_Calc_TotalHoras` AFTER UPDATE ON `carga_horaria` FOR EACH ROW BEGIN
	UPDATE carga_horaria SET CH_TOTHR = CH_HRINI - CH_HRFIM;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cursos`
--

CREATE TABLE `cursos` (
  `CUR_COD` int(10) NOT NULL,
  `CUR_NOME` varchar(50) DEFAULT NULL,
  `CUR_COORDENA` varchar(10) DEFAULT NULL,
  `CUR_NOMCOORD` varchar(50) NOT NULL,
  `CUR_NMDIR` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `cursos`
--

INSERT INTO `cursos` (`CUR_COD`, `CUR_NOME`, `CUR_COORDENA`, `CUR_NOMCOORD`, `CUR_NMDIR`) VALUES
(1, 'Analise e Desenvolvimento de Sistemas ', 'PF0000001', 'Giovani', NULL),
(2, 'Licenciatura em Matemática', 'PF0000007', 'Aline', NULL),
(3, 'Tecnologia em Automação Industrial', 'PF0000012', 'Carla', NULL),
(4, 'Bacharel em Engenharia de Controle e Automação', 'PF0000020', 'Lima', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `materias`
--

CREATE TABLE `materias` (
  `MAT_ID` varchar(11) NOT NULL,
  `MAT_NMDISCIP` varchar(50) NOT NULL,
  `MAT_ANOLET` int(11) NOT NULL,
  `MAT_SEMLET` int(11) NOT NULL,
  `MAT_CARHR` int(11) NOT NULL,
  `MAT_TOTHR` int(11) NOT NULL,
  `MAT_TOTPEN` int(11) NOT NULL,
  `MAT_CURSO` int(10) NOT NULL,
  `MAT_CODUSR` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `materias`
--

INSERT INTO `materias` (`MAT_ID`, `MAT_NMDISCIP`, `MAT_ANOLET`, `MAT_SEMLET`, `MAT_CARHR`, `MAT_TOTHR`, `MAT_TOTPEN`, `MAT_CURSO`, `MAT_CODUSR`) VALUES
('AG1M3', 'Algebra Linear I', 2018, 1, 31, 0, 0, 2, 'PF0000009'),
('AG1M4', 'Algebra Linear II', 2018, 1, 31, 0, 0, 2, 'PF0000010'),
('CIE02', 'Cálculo Diferencial e Integral II', 2018, 1, 63, 0, 0, 4, 'PF0000020'),
('DIDM3', 'Didática ', 2018, 1, 63, 0, 0, 2, 'PF0000011'),
('DTE01', 'Desenho Técnico I', 2018, 1, 63, 0, 0, 4, 'PF0000019'),
('ESTA4', 'Estatítica', 2018, 1, 63, 0, 0, 3, 'PF0000016'),
('FMAA1', 'Fundamentos de Matemática para Automação', 2018, 1, 63, 0, 0, 3, 'PF0000013'),
('FSE01', 'Física I', 2018, 1, 63, 0, 0, 4, 'PF0000018'),
('GE1M1', 'Geometria I', 2018, 1, 63, 0, 0, 2, 'PF0000006'),
('GE2M2', 'Geometria II', 2018, 1, 63, 0, 0, 2, 'PF0000007'),
('GE3M3', 'Geometria III', 2018, 1, 63, 0, 0, 2, 'PF0000008'),
('GTID6', 'Gestão de Tecnologia da Informação', 2018, 1, 63, 0, 0, 1, 'PF0000005'),
('IHCD5', 'Interação Humano Computador', 2018, 1, 31, 0, 0, 1, 'PF0000001'),
('LDSD51801', 'Laboratório e Desenvolvimento de Sistemas', 2018, 1, 63, 100, 50, 1, 'PF0000001'),
('LIPA1', 'Leitura interpretação e Produção de Texto', 2018, 1, 31, 0, 0, 3, 'PF0000012'),
('LP1D5', 'Linguagem de Programação 2', 2018, 1, 63, 0, 0, 1, 'PF0000004'),
('LP2D5', 'Linguagem de Programação 2', 2018, 1, 63, 0, 0, 1, 'PF0000002'),
('PE1D5', 'Topicos Especiais 1', 2018, 1, 63, 0, 0, 1, 'PF0000003'),
('QEE03', 'Química Experimental', 2018, 1, 31, 0, 0, 4, 'PF0000017'),
('SDTA2', 'Sistemas Digitais - Teoria e Prática', 2018, 1, 31, 0, 0, 3, 'PF0000015'),
('TMCA1 ', 'Tecnologia Mecânica ', 2018, 1, 31, 0, 0, 3, 'PF0000014');

-- --------------------------------------------------------

--
-- Estrutura da tabela `ocorrencia`
--

CREATE TABLE `ocorrencia` (
  `OCOR_NUMCHA` int(10) NOT NULL,
  `OCOR_TITULO` varchar(30) NOT NULL,
  `OCOR_REQ` varchar(10) NOT NULL,
  `OCOR_REQNOME` varchar(50) NOT NULL,
  `OCOR_AREA` int(2) NOT NULL,
  `OCOR_STATUS` int(2) NOT NULL,
  `OCOR_DATA` datetime(6) NOT NULL,
  `OCOR_PATRIM` varchar(40) NOT NULL,
  `OCOR_LOCAL` varchar(10) NOT NULL,
  `OCOR_DISCIPLINA` varchar(40) NOT NULL,
  `OCOR_JUST` varchar(60) NOT NULL,
  `OCOR_DESCRI` text,
  `OCOR_COMENT` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ocorrencia`
--

INSERT INTO `ocorrencia` (`OCOR_NUMCHA`, `OCOR_TITULO`, `OCOR_REQ`, `OCOR_REQNOME`, `OCOR_AREA`, `OCOR_STATUS`, `OCOR_DATA`, `OCOR_PATRIM`, `OCOR_LOCAL`, `OCOR_DISCIPLINA`, `OCOR_JUST`, `OCOR_DESCRI`, `OCOR_COMENT`) VALUES
(1, 'teste', 'PF0000001', 'GIOVANNI', 1, 1, '2018-05-29 00:00:00.000000', 'teste', 'teste_loc', 'Teste_disc', 'Teste_just', 'teste teste teste', 'teste teste teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `USR_PRONT` varchar(10) NOT NULL,
  `USR_NOME` varchar(50) DEFAULT NULL,
  `USR_EMAIL` varchar(30) DEFAULT NULL,
  `USR_SENHA` varchar(8) DEFAULT NULL,
  `USR_PERFIL` varchar(15) NOT NULL,
  `USR_STATUS` int(2) DEFAULT NULL,
  `USR_CPF` varchar(12) DEFAULT NULL,
  `USR_CEP` varchar(8) NOT NULL,
  `USR_FONE` varchar(11) DEFAULT NULL,
  `USR_UF` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`USR_PRONT`, `USR_NOME`, `USR_EMAIL`, `USR_SENHA`, `USR_PERFIL`, `USR_STATUS`, `USR_CPF`, `USR_CEP`, `USR_FONE`, `USR_UF`) VALUES
('AUT0000001', 'Dalva', 'dalva@ifsp.com.br', '1234', '1', 1, NULL, '', NULL, ''),
('AUT0000002', 'Cicero', 'cicero@ifsp.com.br', '1234', '3', 2, NULL, '', NULL, ''),
('AUT0000003', 'Tales', 'tales@ifsp.com.br', '1234', '2', 1, NULL, '', NULL, ''),
('CAE0000001', 'Laura', 'laura@ifsp.com.br', '1234', '4', 1, NULL, '', NULL, ''),
('CAE0000002', 'Kelvin', 'kelvin@ifsp.com.br', '1234', '4', 1, NULL, '', NULL, ''),
('CAE0000003', 'Larissa', 'larissa@ifsp.com.br', '1234', '4', 1, NULL, '', NULL, ''),
('PF0000001', 'Giovanni ', 'giovanni@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000002', 'Prado', 'prado@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000003', 'Marcos', 'marcos@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000004', 'Galvão', 'galvao@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000005', 'Franciele', 'franciele@ifsp@gmail.com', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000006', 'Juliana', 'julia@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000007', 'Aline', 'aline@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000008', 'Carol', 'carol@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000009', 'Julia', 'julia@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000010', 'Andre', 'andre@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000011', 'Geovana', 'geovana@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000012', 'Carla', 'carla@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000013', 'Keila', 'keila@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000014', 'Tamara', 'tamara@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000015', 'Renata', 'renata@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000016', 'Leonardo', 'leonardo@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000017', 'Vitor', 'vitor@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000018', 'Daniel', 'daniel@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000019', 'Jane', 'jane@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('PF0000020', 'Lima', 'lima@ifsp.com.br', '1234', '5', 1, NULL, '', NULL, ''),
('sdsd', 'sdsd', 'sdsd', 'd41d8cd9', '', NULL, NULL, '', NULL, ''),
('SOC0000001', 'Francisca', 'francisca@ifsp.com.br', '1234', '3', 1, NULL, '', NULL, ''),
('SOC0000002', 'Maria', 'maria@ifsp.com.br', '1234', '3', 1, NULL, '', NULL, ''),
('SOC0000003', 'Joaquina', 'joaquina@ifsp.com.br', '1234', '3', 1, NULL, '', NULL, ''),
('TI0000001', 'Juca', 'juca@ifsp.com.br', '1234', '1', 1, NULL, '', NULL, ''),
('TI0000002', 'João', 'joao@ifsp@gmail.com', '1234', '1', 1, NULL, '', NULL, ''),
('TI0000003', 'Vania', 'vania@ifsp.com.br', '1234', '1', 1, NULL, '', NULL, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acompanhamento`
--
ALTER TABLE `acompanhamento`
  ADD PRIMARY KEY (`ACOM_ID`),
  ADD KEY `ACOM_USER` (`ACOM_USER`),
  ADD KEY `acompanhamento_ibfk_2` (`ACOM_OCOR`);

--
-- Indexes for table `carga_horaria`
--
ALTER TABLE `carga_horaria`
  ADD PRIMARY KEY (`CH_IDDIARIO`),
  ADD KEY `cargahoraria_ibfk_1` (`CH_CODUSR`),
  ADD KEY `cargaH_ibfk_2` (`CH_IDMAT`);

--
-- Indexes for table `cursos`
--
ALTER TABLE `cursos`
  ADD PRIMARY KEY (`CUR_COD`),
  ADD KEY `CUR_COORDENA` (`CUR_COORDENA`);

--
-- Indexes for table `materias`
--
ALTER TABLE `materias`
  ADD PRIMARY KEY (`MAT_ID`),
  ADD KEY `MAT_CURSO` (`MAT_CURSO`),
  ADD KEY `materias_ibfk_2` (`MAT_CODUSR`);

--
-- Indexes for table `ocorrencia`
--
ALTER TABLE `ocorrencia`
  ADD PRIMARY KEY (`OCOR_NUMCHA`),
  ADD KEY `OCOR_REQ` (`OCOR_REQ`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`USR_PRONT`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acompanhamento`
--
ALTER TABLE `acompanhamento`
  MODIFY `ACOM_ID` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `carga_horaria`
--
ALTER TABLE `carga_horaria`
  MODIFY `CH_IDDIARIO` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acompanhamento`
--
ALTER TABLE `acompanhamento`
  ADD CONSTRAINT `acompanhamento_ibfk_1` FOREIGN KEY (`ACOM_USER`) REFERENCES `usuarios` (`USR_PRONT`),
  ADD CONSTRAINT `acompanhamento_ibfk_2` FOREIGN KEY (`ACOM_OCOR`) REFERENCES `ocorrencia` (`OCOR_NUMCHA`);

--
-- Limitadores para a tabela `carga_horaria`
--
ALTER TABLE `carga_horaria`
  ADD CONSTRAINT `cargaH_ibfk_2` FOREIGN KEY (`CH_IDMAT`) REFERENCES `materias` (`MAT_ID`),
  ADD CONSTRAINT `cargahoraria_ibfk_1` FOREIGN KEY (`CH_CODUSR`) REFERENCES `usuarios` (`USR_PRONT`);

--
-- Limitadores para a tabela `cursos`
--
ALTER TABLE `cursos`
  ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`CUR_COORDENA`) REFERENCES `usuarios` (`USR_PRONT`);

--
-- Limitadores para a tabela `materias`
--
ALTER TABLE `materias`
  ADD CONSTRAINT `materias_ibfk_1` FOREIGN KEY (`MAT_CURSO`) REFERENCES `cursos` (`CUR_COD`),
  ADD CONSTRAINT `materias_ibfk_2` FOREIGN KEY (`MAT_CODUSR`) REFERENCES `usuarios` (`USR_PRONT`);

--
-- Limitadores para a tabela `ocorrencia`
--
ALTER TABLE `ocorrencia`
  ADD CONSTRAINT `ocorrencia_ibfk_1` FOREIGN KEY (`OCOR_REQ`) REFERENCES `usuarios` (`USR_PRONT`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
