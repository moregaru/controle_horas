<?php

  include 'config/menu.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"  >
<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Carga horaria
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
            <li class="active"><a href="materias">Disciplinas</a></li>
            <li class="active">Registro de C.H</li>
        </ol>
    </section>
  </br>
<!-- Main row -->
    <div class="row" width="70%">
    <!-- Left col -->
        <div class="col-md-8">
        <!-- /.box -->
         <div class="row" >
            <div class="col-md-6" > 
           
			<!-- ADICIONAR FUNCIONALIDADE-->
            <div class="container" >
            <br>
            <br>
            <br>
            <br>
            <div class="container">
            <div class="row">
            <div class="col-md-12" align="center">
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#criar">
			    <span class="glyphicon glyphicon-plus"> Registrar
            </span></button>
    
            <div class="container">
                <div class="row">
                    <div class="col-sm-3" align="">
                        <div class="float-label-control">
                            <label for="descricao">Pesquisar por Data
                              <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="xxx."></i>
                             </label>
                            <input type="Date" class="form-control" name="search" id="data" >
                        </div>
                     </div>
        
                    <div class="col-sm-1" align="right">
                        <span class="float-label-control">
                            <label for="data">&nbsp;</label>
                            <input type="button" class="form-control btn-primary" value="Filtrar">
                        </span>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="container">
			    <div class="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%">
                        70%
                    </div>
			    </div>
		    </div>
			<table class="table" align="right">
            <thead>
                <tr>
                    <th>Diario</th>
                    <th>Disciplina</th>
                    <th>Professor</th>
                    <th>Data</th>
                    <th>Hora inicial</th>
                    <th>Hora Final</th>
                    <th>Total</th>
                   
                </tr>
            </thead>
            <tbody>
            <?php if ($chr == FALSE): ?>
			    <tr><td colspan="2">Nenhum registro encontrado</td></tr>
			<?php else: ?>
			<?php foreach ($chr as $row): ?>
			    <tr>
                    <td><?= $row['CH_IDDIARIO'] ?></td>
                    <td><?= $row['CH_IDMAT'] ?> - <?=$row['CH_NMDISC'] ?></td>
                    <td><?= $row['CH_NOMUSR'] ?></td>
                    <td><?= $row['CH_DATA'] ?></td>
                    <td><?= $row['CH_HRINI'] ?></td>
                    <td><?= $row['CH_HRFIM'] ?></td>
                    <td><?= $row['CH_TOTPEN'] ?></td>
                    <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editar">
					<span class="glyphicon glyphicon-pencil"></span></button>
                 </tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
 
    <div id="criar" class="modal fade" role="dialog"  method="post" action="<?=base_url('salvar')?>" enctype="multipart/form-data">>
      <div class="modal-dialog">
        <div class="panel panel-default">
            <div class="panel-heading">Novo Registro
                <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                     <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" method="post" action="<?=base_url('index.php/carga_horaria/salvar')?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Nome usuario</label>
                    <div class="col-md-4">
                        <input id="ch_nomusr" name="ch_nomusr" value="Giovanni" type="text" class="form-control input-md" disabled="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Codigo materia</label>
                    <div class="col-md-4">
                        <input id="ch_idmat" name="ch_idmat" value="IHCD5" type="text" class="form-control input-md" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Codigo professor</label>
                    <div class="col-md-4">
                        <input id="ch_codusr" name="ch_codusr" value="PF0000001" type="text" class="form-control input-md">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Data</label>
                    <div class="col-md-4">
                        <input id="ch_data" name="ch_data" type="date" class="form-control input-md" value="<?=set_value('CH_DATA')?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Hora Inicial</label>
                    <div class="col-md-4">
                        <input id="ch_hrini" name="ch_hrini" type="text" class="form-control input-md" value="<?=set_value('CH_HRINI')?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="textarea">Hora Final</label>
                    <div class="col-md-4">
                        <input id="ch_hrfim" name="ch_hrfim" type="text" class="form-control input-md"  value="<?=set_value('CH_HRFIM')?>" required="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4 control-label" for="button1id"></label>
                    <div class="col-md-8">
                        <button id="ch_conc" class="btn btn-success" value="Salvar" >Enviar</button>
                        <button id="button2id" type="reset" class="btn btn-danger">Cancelar</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    </div>

    <div id="editar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="panel panel-default">
                <div class="panel-heading">Editar Registro #1142
                    <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Nome usuario</label>
                        <div class="col-md-4">
                            <input id="ch_nomusr" name="ch_nomusr" value="Giovanni" type="text" class="form-control input-md" disabled="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Codigo materia</label>
                        <div class="col-md-4">
                            <input id="ch_idmat" name="ch_idmat" value="IHCD5" type="text" class="form-control input-md" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Codigo professor</label>
                        <div class="col-md-4">
                            <input id="ch_codusr" name="ch_codusr" value="PF0000001" type="text" class="form-control input-md">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Data</label>
                        <div class="col-md-4">
                            <input id="ch_data" name="ch_data" type="date" value="2018-04-04" class="form-control input-md" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Hora Inicial</label>
                        <div class="col-md-4">
                           <input id="ch_hrini" name="ch_hrini" type="text" value="19:30" class="form-control input-md" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textarea">Hora Final</label>
                        <div class="col-md-4">
                            <input id="ch_hrfim" name="ch_hrfim" type="text" value="22:30" class="form-control input-md" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="button1id"></label>
                        <div class="col-md-8">
                            <button id="ch_conc" name="ch_conc" class="btn btn-success">Enviar</button>
                            <button id="button2id" type="reset" name="button2id" class="btn btn-danger">Cancelar</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
          </div>
        </div>
       </div>
    </div>
    <br>
    <br>
                  
    <table="center" width="50%">                        
    <thead>
        <i class="fa fa-bar-chart" style="font-size:18px; color:rgb(0, 153, 255)"></i>
        <tr><b><th>Total Pendente: </th></b>
        <?php $tot_pen = 0 ?>
            <?php if ($chr == FALSE): ?>
		        <tr><td colspan="2">--/td></tr>
			<?php else: ?>
			    <?php foreach ($chr as $row): ?>
                    <?php $tot_pen += ($row['CH_TOTPEN'])  ?>
				<?php endforeach; ?>
                <tr>
               <td><?php echo $tot_pen ?></td>
              <td>
            </tr>
		    <?php endif; ?>
        <!--<td>1:15:00</td> -->
    <br>
    <br>
    <i class="fa fa-line-chart" style="font-size:18px; color:rgb(0, 153, 255)"></i>
    <tr><b><th>Adicional Noturno: </th></b>
        <?php $soma = 0 ?>
        <?php if ($chr == FALSE): ?>
		    <tr><td colspan="2">--/td></tr>
		<?php else: ?>
            <?php foreach ($chr as $row): ?>
                <?php $soma += ($row['CH_ADDNOT'])  ?>
            <?php endforeach; ?>
	    <tr>
        <td><?php echo $soma ?></td>
        <td>
    </tr>
		<?php endif; ?>
    </div>
    </tr>
    </thead>
</table>
<br>    
<br>
<br>        
<br>
<br>
<footer>
                 
<br>
<br>    
<br>
<br>        
<br>
<br>
</footer>
</div>

   
            <!-- /.table-responsive -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
            <!-- /.info-box-content -->
          </div>
            <!-- /.footer -->
          </div>
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

<?php

  include 'config/rodape.php';

?>
