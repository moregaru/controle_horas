<?php

include 'config/menu.php';

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"  >
<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Área de Cadastro - FIEC
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
        </ol>
    </section>
  </br>
<!-- Main row -->
    <div class="row" width="70%">
    <!-- Left col -->
        <div class="col-md-8">
        <!-- /.box -->
         <div class="row" >
            <div class="col-md-6" > 
            

<!--TITULO DO ICONE NA URL-->
<title>FIEC - Cadastro</title>
<br><br>
<center><img src="https://cdn.icon-icons.com/icons2/38/PNG/512/adduser_4961.png" class="img-responsive" alt="" width="150px"/>
  <div class="container">
    <div class="card card-register mx-auto mt-5">
     <B> <div class="card-header"> <h4>Fluxo de Informacões Educacionais e Correlatas</h4> </div> </B>
     <B> ÁREA DE CADASTRO </B>
     <br>
     <br>
      <div class="card-body">
       
 		<!--espaço nome-->
        <center><form name="formCadastro" method="post" action="usuarios/novo">

        <div class="form-group">
        <label for="sel1">Selecione:</label>
        <select class="form-control" id="USR_PERFIL">
            <option value="1">TI</option>
            <option value="2">AUT</option>
            <option value="3">CAE</option>
            <option value="4">SOC</option>
        </select>
        </div>
    
        <div class="form-group">
            <label for="textNome" class="control-label">Nome:</label>
            <input type="text" id="USR_NOME" name="USR_NOME" class="form-control" style="min-width:300px;" placeholder="Nome completo" >
        </div>
                                 
        <!--espaço CPF-->
        <div class="form-group">
            <label for="textCpf" class="control-label">CPF:</label>
            <input type="text" id="USR_CPF" name="USR_CPF" class="form-control cpf-mask" size="11" maxlength="11" placeholder="Digite seu CPF..." pattern="[0-9]{11}"> 
        </div>
        
          <!--espaço Prontuário-->
          <div class="form-group">
            <label for="textPront" class="control-label">Prontuário:</label>
            <input type="text" id="USR_PRONT" name="USR_PRONT" class="form-control" size="11" maxlength="11" placeholder="Digite seu Prontuário..." pattern="{11}"> 
        </div>
        
        <!--telefone-->		
        <div class="form-group">
            <label for="textTelefone" class="control-label">Telefone:</label>
            <input class="form-control"  id="USR_FONE" name="USR_FONE"  placeholder="Ex.: (ddd) xxxx-xxxx" autocomplete="off" type="text" size="11" maxlength="11">
        </div>
        
               
        <!--cep-->			
        <div class="form-group">
            <label for="textCep" class="control-label">CEP:</label>
            <input type="text" id="USR_CEP" name="USR_CEP" class="form-control" pattern="[0-9]{8}" size="8" maxlength="8" placeholder="Digite seu CEP...">
        </div>
        
             
        <!--uf-->	
        <div class="form-group">
            <label for="textEstado">Estado:</label>
            <select class="form-control" id="USR_UF">
                <option>Selecionar:</option>
                <option value="AC">Acre</option>
                <option value="AL">Alagoas</option>
                <option value="AP">Amapá</option>
                <option value="AM">Amazonas</option>
                <option value="BA">Bahia</option>
                <option value="CE">Ceará</option>
                <option value="DF">Distrito Federal</option>
                <option value="ES">Espírito Santo</option>
                <option value="GO">Goiás</option>
                <option value="MA">Maranhão</option>
                <option value="MT">Mato Grosso</option>
                <option value="MS">Mato Grosso do Sul</option>
                <option value="MG">Minas Gerais</option>
                <option value="PA">Pará</option>
                <option value="PB">Paraíba</option>
                <option value="PR">Paraná</option>
                <option value="PE">Pernambuco</option>
                <option value="PI">Piauí</option>
                <option value="RJ">Rio de Janeiro</option>
                <option value="RN">Rio Grande do Norte</option>
                <option value="RS">Rio Grande do Sul</option>
                <option value="RO">Rondônia</option>
                <option value="RR">Roraima</option>
                <option value="SC">Santa Catarina</option>
                <option value="SP">São Paulo</option>
                <option value="SE">Sergipe</option>
                <option value="TO">Tocantins</option>
            </select>
        </div>

        
        <!--espaço e-mail-->              
        <div class="form-group">
            <label for="textEmail" class="control-label">Email:</label>
            <input type="Email" id="USR_EMAIL" name="USR_EMAIL" class="form-control" placeholder="Digite seu E-mail...">
        </div>
        
        <!--espaço senha-->
        <div class="form-group">
            <label for="textPassword" class="control-label">Senha:</label> 
           <input type="password" id="USR_SENHA" name="USR_SENHA" class="form-control" size="6" maxlength="6" placeholder="Digite sua Senha..." pattern="{6}">
           
        </div>
      
        <br>      
        <!-- Botão entrar -->
        <div class="form-group">
            <button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar</button>
        </div>
        <br>
        <br>
                
      </div>
    </div>
  </div>
<br>
<br>

<!-- /.table-responsive -->
</div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
            <!-- /.info-box-content -->
          </div>
            <!-- /.footer -->
          </div>
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

 <?php
    include 'config/rodape.php';
?>
  
