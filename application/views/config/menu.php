<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema | CFIEC</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url()."assets/";?>dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
      

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Sistema </b>CFIEC</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
              <!--<img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Nome Usuario</span>-->
              <a href='login/logout'>Encerrar Sessão</a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="usuarios" data-toggle="control-sidebar"><i class="fa fa-user-plus"></i></a>
          </li>
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
          
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAclBMVEUAAAD////Pz8+GhoZWVlZdXV0UFBRVVVXj4+NRUVHt7e3Jycnl5eX7+/s5OTnCwsJERETU1NQsLCx7e3ulpaWTk5Nvb29kZGT29vYODg60tLQaGhqhoaHs7Oy7u7uLi4t4eHgjIyM1NTVAQECioqInJycZkGWDAAAGmUlEQVR4nO2di1bqOhRFAwLyfh1QBEXUc/7/Fy/QW6DQQs2eC9qOzA/QzFHSPLqz4mp3YLCoL2fvT6+daeut9TUdjp+bn7N1ezG4xz936n/QHz01Wi6dVWez7KsbIDUc9P4MM+SODDejrrIRQsP2n5t2MY2Rrhkyw9FPbr8976oHKTJc/9Jvx3whaYrEsP38e78tq5miMQrDTy+/HWPBm5U3bI+9Bbcs8fbghmuL35Y/dINow5lR0LlnuEWwoX8XPDJm36ms4RwQdO4Hna+ihvafaMQr2SjSsAcJOvcJtgo0/MAEnVtzzQIN/SYy6ay4tw1nSHXCiBesXZhhFxV0rk41DDN8hQ1/qIZRhm1Y0DlqUUwZ0o9wO7WBWgYZ8o/QuQnTNMiQmI+eA60yGMPBSmDomDGRMeTma6cwq2HGMP++4W9gRn3GUCLooLYRf6QvMkTmNYihdW8mi3eicYihphs61yQahxje/vzixz+icYRh901k6IgNYsJwohJEJm6EYV1mSKwvCMOlzJB4mRKGzC5pGsTkmzDcyAyJeRthyK9+Y4itYcKwKTMkhvxiGzaA1gXDPATDYBgMbQTDPATDYFh8w0YwDIbB0EQwzEMwLL8hWSoUDH9PMMxDMPSHqIgmDDvBMBhmUpB+uJCUC+2ZAkVDgCFbOpsEKBqyG9Kls0k+CmD4IjXcPN6QrNBPw1ysYDZU1F2eYv4MbDUcTMWG00cbKoqDk1grTqyGujqMGOvRWavhk9zw6cGGG7mhtR7DaqgdDYPhPQz1/dA6q7EavssNrbVtVkNd5WXMo8fDxZdY8M2aCGKel+o+j0aYS9vMhprjMkfMRbT29aF26m2eeAOGI6lhrwCG0kEfKL8k9tp0v9MhkB9B7Jf2VSNGqyjnLVQnuxBB6HRePytzzsKKSf+CzgErTq9Bx9Uhw8U/XPALisagzuPzQ4Z9LziCMuQnb1QwBpaLQZ/QW1ENwwzpvW8sZggzpLeG21TDuAQeU9TeBfY1RQxnyO5+cyGRnCH6pbSQOVHou2bONQs0JB8iGEhLJtJxu8NkACZpuMAM7fUJR9DcRKoncmFtNdiQeohoLDSb7skUDyGRJgfghFZiYvPNxu3DhsTs1L5FmoDOEba/bJBQmhPwLGjzOpEcKXbghtagEzzQm08s/2sSpHZnjghS5y0xGfB7dIfAcGDYHsZW9kcUNwf4Dxl8qr7o9gffdDM0xztGYui7jLKWsKUSDL0IhsEQJRh6EQyDIYrEcONpyC8saiJD39UFvbzfIzH0PXJJReknkBh6CkLx1ueNEfxN/41hxe15CkP/ugx4I3GPwtA/sRX8bHhAYegffj0UtEZgaDlVSm+W1iSGlkMm7EeZPQJDjytID2A3BB3hDW2V7fy1ubyhLUOig7cHN7TW0oJ3ykXghkZBfuZG/0H7VRf0VauwIVHvDf9OWUOmBJP9PIMadplDCa3CVptgx4OI4J0DoGGfO/80Be/n5gzrZC37G3aFJWdIX+OBLRUhwzpb5b1jDD1GxLCtOWT5ggwbgGFdeEcJ8ByNht3epy6tbcfqs2ccHfMb9usJeuvZ/EWXtndK52U+W/eS/z7/cJLXsKeL0vWjkXfnMZ+hMlbPn3ynTnIYDpbq7AtfvpY5isRuGy6/Hy1yhe/bVVS3DD/u8zLxp3Nri/WGYTE7YJIb3fGqYZufiykYX537XDPUJyRRXNsqzzZcFL0HntLJXjNnGk4UMQk6WplxUlmG+jxEmqxxI8NQdy2ljow1c7qhPklPQfqRt1RD3YJPS2oVdZqhOqBMR1rJUYphmUaJc1I+zl0YDsosuFW8WG1cGJa1D8Zc9MVzQ9VF8Pfj/OvcmWEZx8Fz5tcMyzeTSWOZbagPI70PkyxD4UUV9yWRG3JqWI71bh7G6Yb60OP78ZRmqE3pvDejS8PKdMKIY1c8GJZ9LnPO67lhNUbCU5ZJQ+11OI+hmzAs75Iwm+apYbXeozGjo6H8spjHMB0cDKuwokhjHhuqr2x6HB//G1bxNRPRjAyrsmZKY7I31F2y+Xied4bqyw0eS29rWO7dw1t0as6aelR06q46C/t0qu4XCAQCgUAgEAgE7kW5iix/T8v5B5GUg6Gr7lZiRNOVoRrfwszpr39/LG1nSAQqA8Oaq+iXtZjRbs+7aOfuSBr7XX0oCaGI7NIZdt+eqvtpZhJ/IS3Z6ZG8RKdMou/43Sr2xUY3UU8zqtqgMYzrvo51be1Zc1iNX2tr2JwdD+z9BxrdgE4HpkvcAAAAAElFTkSuQmCC"
           class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Bem-vindo(a) <?php $user = $this->session->userdata("usuario"); echo $user."!";?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Pesquisar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU DE NAVEGAÇÃO</li>
        <li class="active treeview menu-open">
          <a href="materias">
            <i class="fa fa-dashboard"></i> <span>Carga Horária</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="active">
            <li><a href="carga_horaria"><i class="fa fa-circle-o"></i> Registro de C.H</a></li>
          </ul>
        </li>
        <li>
          <a href="reuniao">
            <i class="fa fa-files-o"></i>
            <span>Registro de Cursos</span>
          </a>
        </li>
        <li>
          <a href="ocorrencia">
            <i class="fa fa-th"></i> <span>Registro de Ocorrências</span>
          </a>
        </li>
        <li>
            <a href="calendar">
            <i class="fa fa-calendar"></i> <span>Calendário</span>
          </a>
        </li>
        <li>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>