<?php

  include 'config/menu.php';
  include 'css_add.php';

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Registro de ocorrências
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
        <li class="active"><a href="ocorrencia">Registro de ocorrências</a></li>
        <li class="active">Ocorrências</li>
      </ol>
    </section>
  </br>
  <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">    
          <!-- /.box -->
          <div class="row">
            <div class="col-md-6">
            <!-- ADICIONADO OCORRENCIA -->
            <br>
    <br>
    <div class="container">
        <div class="panel panel-default" >
            <div class="panel-heading">
            <?php if ($ocorrencia == FALSE): ?>
					<tr><td colspan="2">Nenhum contato encontrado</td></tr>
				<?php else: ?>
					<?php foreach ($ocorrencia as $row): ?>
                ID Chamado: #<td><?= $row['OCOR_NUMCHA'] ?></td>
            </div>
            <div class="panel-body">
                <h3>Dados da ocorrência</h3>
                <hr>
                <form class="form-horizontal">
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Patrimônio</label>
                        <div class="col-md-4">
                            <input id="OCOR_PATRIM" name="OCOR_PATRIM" type="text" class="form-control input-md" value="<?= $row['OCOR_PATRIM'] ?>" disabled="">
                        </div>
                    </div>

                   <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Local</label>
                        <div class="col-md-4">
                            <input id="OCOR_LOCAL" name="OCOR_LOCAL" type="text" class="form-control input-md" value="<?=$row['OCOR_LOCAL']?>" disabled="">
                        </div>
                    </div>
                    
                   <!-- Text input-->
                   <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Requerente</label>
                        <div class="col-md-4">
                            <input id="OCOR_REQ" name="OCOR_REQ" type="text" class="form-control input-md" value="<?=$row['OCOR_REQ']?>" disabled="">
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Disciplina</label>
                        <div class="col-md-4">
                            <input id="OCOR_DISCIPLINA" name="OCOR_DISCIPLINA" type="text" class="form-control input-md" value="<?=$row['OCOR_DISCIPLINA']?>" disabled="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="selectbasic">Status</label>
                        <div class="col-md-4">
                            <select id="OCOR_STATUS" name="OCOR_STATUS" class="form-control" required="" value="<?=$row['OCOR_STATUS']?>" disabled="" >
                          <option value="1">Registrado</option>
                          <option value="2">Pendente</option>
                          <option value="3">Em execução</option>
                          <option value="4">Encerrado</option>
                        </select>
                        </div>
                    </div>
                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Titulo</label>
                        <div class="col-md-4">
                            <input id="OCOR_TITULO" name="OCOR_TITULO" type="text" class="form-control input-md" value="<?=$row['OCOR_TITULO']?>" disabled="">
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Justificativa</label>
                        <div class="col-md-4">
                            <input id="OCOR_JUST" name="OCOR_JUST" type="text" class="form-control input-md" value="<?=$row['OCOR_JUST']?>" disabled="">
                        </div>
                    </div>

                    <!-- Textarea -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textarea">Descrição</label>
                        <div class="col-md-4">
                            <textarea class="form-control" id="OCOR_DESCRI" name="OCOR_DESCRI" value="<?=$row['OCOR_DESCRI']?>" disabled=""></textarea>
                        </div>
                        <?php endforeach; ?>
                        <?php endif; ?>
                      <br>
                      <br> 
                      <br>
                      <br>
                      <br>
                      <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4" alight="center">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#altModal">Alterar</button>
                            <a href="<?= $row['excluir_url'] ?>">
                              <button id="excluir_url" type="reset" name="excluir_url" class="btn btn-danger btn-lg">Deletar</button>
                            </a>
                        </div>
                    </div>
                    </div>

                    
                </form>

                <hr>

                <h3>Atualizações</h3>

                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textarea">Comentário</label>
                        <div class="col-md-4">
                            <textarea class="form-control" id="textarea" name="textarea"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Adicionar comentario</button>
                            <button id="button2id" type="reset" name="button2id" class="btn btn-danger btn-lg">Limpar</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="row container">
                    <div class="col-sm-1">
                        <div class="thumbnail">
                            <img class="img-responsive user-photo" src="https://ssl.gstatic.com/accounts/ui/avatar_2x.png">
                        </div>
                        <!-- /thumbnail -->
                    </div>
                    <!-- /col-sm-1 -->
                    <div class="col-sm-5">
                    <div class="panel panel-default">
                        <div class="panel-heading comment">
                            Fulano - 23/03/2018 21:01
                        </div>
                        <div class="panel-body">
                            AAAAAAAAAAAAAAAAAA
                                <br>
                            Status:<strong> Pendente >> Em Execução </strong>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
            <!-- /.table-responsive -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
            <!-- /.info-box-content -->
          </div>
            <!-- /.footer -->
          </div>
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
  <?php
    include 'config/rodape.php';
  ?>
  