<?php

  include 'config/menu.php';

?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"  >
<!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Calendario
            <small>Version 1.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
            <li class="active">Calendario</li>
        </ol>
    </section>
  </br>
<br>
<center><img src ="<?php echo base_url()."assets/";?>/image/logo-ifsp.png" width="420px" height="140px"></center>
<br>

    <center><h2> Calendário Acadêmico </h2><br />
    <p> Clique abaixo para visualizar: </p></center>




<!-- Calendário 1 -->
<center>    <div class="container">

    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#concsub"> Cursos Técnico </button>
    <div id="concsub" class="collapse"><br />

<iframe src="<?php echo base_url()."assets/";?>/pdf/concsub.pdf" width="600" height="780" style="border: none;"></iframe>
  
    </div>
    </div><br />

<!-- Calendário 2 -->
    <div class="container">

    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#integrado"> Técnico Integrado </button>
    <div id="integrado" class="collapse"><br />
  
<iframe src="<?php echo base_url()."assets/";?>/pdf/integrado.pdf" width="600" height="780" style="border: none;"></iframe>
  
    </div>
    </div><br />

<!-- Calendário 3 -->
    <div class="container">

    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#superior"> Cursos Superiores </button>
    <div id="superior" class="collapse"><br />
  
<iframe src="<?php echo base_url()."assets/";?>/pdf/superior.pdf" width="600" height="780" style="border: none;"></iframe>
  
    </div>
    </div><br />
 
<!-- Calendário 4 -->
    <div class="container">

    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#posgra"> Pós-graduação </button>
    <div id="posgra" class="collapse"><br />
  
<iframe src="<?php echo base_url()."assets/";?>/pdf/posgra.pdf" width="600" height="780" style="border: none;"></iframe>
</center> 
    </div>
    </div><br />
