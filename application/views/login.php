<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema | CFIEC</title>

  <link rel="icon" href="/assets/imagens/teste.png">

  
 

    <!-- Google Font -->

  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        
      </head>
<body class="hold-transition skin-blue sidebar-mini">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<!------ Include the above in your HEAD tag ---------->



<div class="container">
  <div class="row" id="pwd-container">
  <div class="col-md-4"></div>
    <div class="col-md-4">
      <section class="login-form">
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <img src="http://portal.ifspguarulhos.edu.br/images/logos/Guarulhos-02.jpg" class="img-responsive" alt="" />
          <br>
          <br>
          <br>
          <br>
          <br>
          <?php
              echo form_open($action='login/autenticar');
              echo form_label($label_text="Prontuário", "USR_PRONT");
              echo form_input(array(

                "name"  => "USR_PRONT",
                "id"    => "USR_PRONT",
                "class" => "form-control",
                "maxlength" => "255" 

              )); 
              
          ?>
          <br>
          <br>
          <?php 
              echo form_label($label_text="Senha", "USR_SENHA");
              echo form_password(array(

                "name"=> "USR_SENHA",
                "id" => "USR_SENHA",
                "class" => "form-control",
                "maxlength" => "255" 

              )); 
          ?>
          <br>
          <?php
            echo form_submit('submit', 'Login'); 
            echo form_close();
          ?>
          <div>
            <a href="usuarios">Crie uma conta</a> ou
            <a href="#">Esqueci a senha</a>
        </div>
      </form>
      <div class="form-links">
        <a href="#">www.website.com</a>
      </div>
      </section>  
    </div>
  </div>
</body>
</html>