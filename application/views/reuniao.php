<?php
  include 'config/menu.php';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper"  >
<!-- Content Header (Page header) -->
    <section class="content-header">
    <br><br>
        <h1>Lista de cursos
            <small>Version 2.0</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
        </ol>
    </section>
  </br>
<div class="container">
	<div class="row">	
		<div class="col-md-12" align="center">
		<!--<button type="button" class="btn btn-success" data-toggle="modal" data-target="#MyModal">
				<span class="glyphicon glyphicon-plus"> Adicionar
			</span></button>-->
        <br><br><br>    
	<table id="mytable" class="table table-bordred table-striped">
	<thead>         
        <!--<th>Código</th>-->
        <th>Curso</th>
        <th>Coordenador Curso</th>
        <th>Diretor</th>
        <th>Gerar Lista</th>
    </thead>
	<tbody>
        <?php if ($cursos == FALSE): ?>
        <tr><td colspan="2">Nenhum registro encontrado</td></tr>
		<?php else: ?>
		<?php foreach ($cursos as $row): ?>
        
        <?php $id = ($row['CUR_COD'])?>
		<tr>
            <td><?= $row['CUR_NOME'] ?></td>
            <td><?= $row['CUR_NOMCOORD'] ?> | <?=$row['CUR_COORDENA'] ?></td>
            <td><?= $row['CUR_NMDIR'] ?></td>
            <td>
            <a href="impressao?id".$id>
                <left><button type="button" class="btn btn-primary hidden-print" value= "$id">
                <span class="glyphicon glyphicon-print "></span></button></left>
                <td>
            </a>
        </tr>
	    <?php endforeach; ?>
		<?php endif; ?>
    </tbody>     
</table>
</div>
</div>
</div>
</div>

</html>




