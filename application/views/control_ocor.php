<?php

  include 'config/menu.php';

?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <br><h1>Registro de ocorrências</h1><br>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Registro de ocorrências</li>
      </ol>
    </section>
  </br>
  <!-- Main row -->
      <div class="row" >
        <!-- Left col -->
        <div class="col-md-8">
          <!-- /.box -->
          <div class="row" >
            <div class="col-md-6" > 
            <!-- ADICIONADO OCORRENCIA -->
            <body>
            <br>
            <br>
            <div class="col-md-4"  align="center">
            <!--<a href="nova_ocorre">-->
              <button type="button"  class="btn btn-primary" data-toggle="modal" data-target="#MyModal">Registrar ocorrência</button>
            <!--</a>-->
            </div>
            <br><br><br><br><br>
            <div class="container">
             <table class="table" width="100%" >
               <thead>
                 <tr>
                  <th width="10%">Chamado</th>
                  <th width="20%">Titulo</th>
                  <th width="20%">Requisitante</th>
                  <th width="15%">Área responsável</th>
                  <th width="13%">Status</th>
                  <th width="10%">Data Registro</th>
                  <th></th>
                </tr>
            </thead>
            <tbody>
            <tbody>
			      	<?php if ($ocorrencia == FALSE): ?>
					      <tr><td colspan="2">Nenhum registro encontrado</td></tr>
				      <?php else: ?>
					    <?php foreach ($ocorrencia as $row): ?>
						  <tr>
               <td><?= $row['OCOR_NUMCHA'] ?></td>
               <td><?= $row['OCOR_TITULO'] ?></td>
               <td><?= $row['OCOR_REQ'] ?></td>
               <td><?= $row['OCOR_AREA'] ?></td>
               <td><?= $row['OCOR_STATUS'] ?></td>
               <td><?= $row['OCOR_DATA'] ?></td>
              <td>
              <a href="registro_ocorre" "<?= $row['OCOR_NUMCHA'] ?>">
                <button id="singlebutton" name="singlebutton" class="btn btn-primary">Abrir</button>
              </a>
              </td>
              </tr>
              <tr>
			 </tr>
				<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
    </div>
  
    
  <div id="MyModal" class="modal fade" role="dialog" enctype="multipart/form-data">
        <div class="modal-dialog">
            <div class="panel panel-default">
                <div class="panel-heading">Nova ocorrência
                <button type="button" class="close" aria-label="Close" data-dismiss="modal">
                  <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="panel-body">
                  <form class="form-horizontal" method="post" action="<?=base_url('index.php/ocorrencia/salvar')?>" enctype="multipart/form-data">
                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="selectbasic">Área</label>
                            <div class="col-md-4">
                                <select id="OCOR_AREA" name="OCOR_AREA" class="form-control">
                                  <option  value="<?=set_value('OCOR_AREA')?>">TI</option>
                                  <option  value="<?=set_value('OCOR_AREA')?>">Tec. Automação</option>
                                  <option  value="<?=set_value('OCOR_AREA')?>">Socio Pedagógica</option>
						                     </select>
                            </div>
                        </div>
                       <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Patrimônio</label>
                            <div class="col-md-4">
                                <input id="OCOR_PATRIM" name="OCOR_PATRIM" class="form-control input-md" type="text" value="<?=set_value('OCOR_PATRIM')?>">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Local</label>
                            <div class="col-md-4">
                                <input id="OCOR_LOCAL" name="OCOR_LOCAL" type="text" class="form-control input-md" value="<?=set_value('OCOR_LOCAL')?>">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Requerente</label>
                            <div class="col-md-4">
                                <input id="OCOR_REQ" name="OCOR_REQ" type="text"  class="form-control input-md" value="<?=set_value('OCOR_REQ')?>">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Disciplina</label>
                            <div class="col-md-4">
                                <input id="COR_DISCIPLINA" name="OCOR_DISCIPLINA" type="text"  class="form-control input-md" value="<?=set_value('OCOR_DISPLINA')?>">
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="OCOR_STATUS">Status</label>
                        <div class="col-md-4">
                            <select id="OCOR_STATUS" name="OCOR_STATUS" class="form-control" value="<?=set_value('OCOR_STATUS')?>" disabled="">
                          <option value="1">Registrado</option>
                          <option value="2">Pendente</option>
                          <option value="3">Em execução</option>
                          <option value="4">Encerrado</option>
                        </select>
                        </div>
                    </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="OCOR_TITULO">Titulo</label>
                            <div class="col-md-4">
                                <input id="OCOR_TITULO" name="OCOR_TITULO" type="text" class="form-control input-md" value="<?=set_value('OCOR_TITULO')?>" required="">
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="OCOR_JUST">Justificativa</label>
                            <div class="col-md-4">
                                <input id="OCOR_JUST" name="OCOR_JUST" type="text"  class="form-control input-md" value="<?=set_value('OCOR_JUST')?>"required="">
                            </div>
                        </div>
                        <!-- Textarea -->
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="OCOR_DESCRI">Descrição</label>
                            <div class="col-md-4">
                                <textarea class="form-control" id="OCOR_DESCRI" name="OCOR_DESCRI" value="<?=set_value('OCOR_DESCRI')?>"></textarea>
                            </div>
                        </div>
                        <!-- Button (Double) -->
                        <div class="form-group">
                            <label class="col-md-4 control-label"></label>
                            <div class="col-md-8">
                                <button id="button2id" type="submit" value="Salvar" class="btn btn-success">Enviar</button>
                              
                                <button id="button2id" type="reset" class="btn btn-danger">Cancelar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /.table-responsive -->
    </div>
    <!-- /.box-footer -->
    </div>
    <!-- /.box -->
    </div>
    <!-- /.col -->
    <!-- /.info-box-content -->
    </div>
    <!-- /.footer -->
          </div>
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
<?php

  include 'config/rodape.php';

?>