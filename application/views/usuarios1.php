<h1> Cadastrar </h1>


<?php
echo form_open('usuarios/novo');

echo form_label($label_text="Prontuário", "USR_SENHA");
echo form_input(array(

  "name"=> "USR_PRONT",
  "id" => "USR_PRONT",
  "class" => "form-control",
  "maxlength" => "255" 

));

echo form_label($label_text="Nome", "USR_PRONT" );
echo form_input(array(

  "name"=> "USR_NOME",
  "id" => "USR_NOME",
  "class" => "form-control",
  "maxlength" => "255" 

));


echo form_label($label_text="E-mail", "USR_EMAIL");
echo form_input(array(

  "name"=> "USR_EMAIL",
  "id" => "USR_EMAIL",
  "class" => "form-control",
  "maxlength" => "255" 
  
));

echo form_label($label_text="Perfil", "USR_PERFIL");
$selectname = "myselect";
   $options = array(
    ""=>'Selecione',
    "1-ADMIN"=>"1-ADMIN",
    "2-TI"=>"2-TI",
    "3-SERVIDOR"=>"3-SERVIDOR",
    "4-DOCENTE"=>"4-DOCENTE"
   );

  // echo form_dropdown( $selectname, $options, set_value( $selectname, $selectname ) );
  echo form_dropdown( $selectname, $options, 'Selecione');

echo form_label($label_text="Senha", "USR_SENHA");
echo form_password(array(

  "name"=> "USR_SENHA",
  "USR_ID" => "USR_SENHA",
  "class" => "form-control",
  "maxlength" => "255" 

));

 echo form_submit('submit', 'Cadastrar'); 


echo form_close();

?>