
<?php

include 'config/menu.php';

?>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Registro de Disciplinas
        <small>Version 2.0</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Registro de disciplinas</li>
      </ol>
    </section>
  </br>
  <!-- Main row -->
      <div class="row" >
        <!-- Left col -->
        <div class="col-md-8">
          <!-- /.box -->
          <div class="row" >
            <div class="col-md-6" > 
            <!-- ADICIONADO OCORRENCIA -->
            <body>

    <br>
    <br>
    <br>
    <br>
    <br>

  <div class="container">
    
      <div class="row">
          <div class="col-sm-9">
                  <div class="float-label-control">
                      <label for="descricao">Pesquisar
                          <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" 
                          data-placement="top" title="xxx."></i>
                      </label>
                      <input type="Text" class="form-control" name="search" id="data" >
                  </div>
              </div>
      
              <div class="col-sm-3">
                  <span class="float-label-control">
                      <label for="data">&nbsp;</label>
                      <input type="button" class="form-control btn-primary" value="Filtrar">
                  </span>
              </div>
      </div>

  <br>
  <br>
  <br>
  <br>
  <table class="table" width="80%">
    <thead>
      <tr>
        <th>Disciplina</th>
        <th>Nome</th>
        <th>Ano</th>
        <th>Semestre</th>
        <th>C. H. Total</th>
        <th>Horas Cumpridas</th>
        <th>Horas Pendentes</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
    <?php if ($materias == FALSE): ?>
					      <tr><td colspan="2">Nenhum registro encontrado</td></tr>
				      <?php else: ?>
					    <?php foreach ($materias as $row): ?>
						  <tr>
               <td><?= $row['MAT_ID'] ?></td>
               <td><?= $row['MAT_NMDISCIP'] ?></td>
               <td><?= $row['MAT_ANOLET'] ?></td>
               <td><?= $row['MAT_SEMLET'] ?></td>
               <td><?= $row['MAT_CARHR'] ?></td>
               <td><?= $row['MAT_TOTHR'] ?></td>
               <td><?= $row['MAT_TOTPEN'] ?></td>
              <td>
            
            <a href="carga_horaria">
	        	<button type="button" class="btn btn-success" data-toggle="modal" data-target="#criar">
			    	<span class="glyphicon glyphicon-plus"> 
			      </span></button>
      </tr>
					<?php endforeach; ?>
				<?php endif; ?>
			</tbody>
		</table>
    </div>
  
  
            <!-- /.table-responsive -->
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
            <!-- /.info-box-content -->
          </div>
            <!-- /.footer -->
          </div>
                <!-- /.item -->
              </ul>
            </div>
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  
<?php

  include 'config/rodape.php';

?>