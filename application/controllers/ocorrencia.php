<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ocorrencia extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("usuario");
		if (empty($user))
		{
			redirect('login');
		}
	}

	public function index()
	{
		// Recupera os contatos através do model
		$this->load->model('Ocorrencia_Model','model');
		$ocorrencia = $this->model->GetAll('OCOR_NUMCHA');
		// Passa os contatos para o array que será enviado à home
		$dados['ocorrencia'] =$this->model->Formatar($ocorrencia);
		// Chama a home enviando um array de dados a serem exibidos
		$this->load->view('control_ocor',$dados);
	}
    
		public function Salvar(){
			// Recupera os contatos através do model
			$this->load->model('Ocorrencia_Model','model');
			$ocorrencia = $this->model->GetAll('OCOR_NUMCHA');
			// Passa os contatos para o array que será enviado à home
			$dados['ocorrencia'] =$this->model->Formatar($ocorrencia);
			
		
			$ocorrencia = $this->input->post();
			// Insere os dados no banco recuperando o status dessa operação
			 $this->model->Inserir($ocorrencia);
			// Checa o status da operação gravando a mensagem na seção
			/*if(!$status){
				$this->session->set_flashdata('error', 'Não foi possível inserir o contato.');
			}else{
				$this->session->set_flashdata('success', 'Contato inserido com sucesso.');
				// Redireciona o usuário para a home
				redirect();
			}*/
			// Carrega a home
			$this->load->view('control_ocor',$dados);
		}
		
		 
}

