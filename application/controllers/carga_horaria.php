<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carga_horaria extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("usuario");
		$acess = $this->session->userdata("acesso");
			
		if (empty($user))
		{
			redirect('login');
		}
		else{
			
			if  ($acess == "1")
			{
				$this->load->view('usuarios/noacess');
				echo "Usuário sem acesso!";
			}
		}
	}
	
	public function index()
	{
		// Recupera os contatos através do model
		$this->load->model('CargaH_Model','model');
		$chr = $this->model->GetAll('CH_IDDIARIO');
		// Passa os contatos para o array que será enviado à home
		$dados['chr'] =$this->model->Formatar($chr);
		// Chama a home enviando um array de dados a serem exibidos
		$this->load->view('carga_horaria',$dados);
	}
	
	public function Horas()
	{
		$this->load->model('CargaH_Model','model');
		$totHoras = $this->model->calculaHR();
	}
	

	public function Salvar(){

		$this->load->model('CargaH_Model','model');
		// Recupera os contatos através do model
		$chr = $this->model->GetAll('CH_IDDIARIO');
		// Passa os contatos para o array que será enviado à home
		$dados['chr'] =$this->model->Formatar($chr);
		// Executa o processo de validação do formulário
		//$validacao = self::Validar();
		// Verifica o status da validação do formulário
		// Se não houverem erros, então insere no banco e informa ao usuário
		// caso contrário informa ao usuários os erros de validação
		//if($validacao){
			// Recupera os dados do formulário
			$chr = $this->input->post();
			// Insere os dados no banco recuperando o status dessa operação
			$this->model->Inserir($chr);
			// Checa o status da operação gravando a mensagem na seção
			/*if(!$status){
				$this->session->set_flashdata('error', 'Não foi possível inserir o contato.');
			}else{
				$this->session->set_flashdata('success', 'Contato inserido com sucesso.');
				// Redireciona o usuário para a home
				redirect();
			}
		/*}else{
			$this->session->set_flashdata('error', validation_errors('<p>','</p>'));
		}*/
		// Carrega a home
		$this->load->view('carga_horaria',$dados);
	}

	


}
