<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materias extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("usuario");
		if (empty($user))
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		// Recupera os contatos através do model
		$this->load->model('Materias_Model','model');
		$materias = $this->model->GetAll('MAT_ID');
		// Passa os contatos para o array que será enviado à home
		$dados['materias'] =$this->model->Formatar($materias);
		// Chama a home enviando um array de dados a serem exibidos
		$this->load->view('materias',$dados);
	}
}
