<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


	public function index()
    {
		$this->load->view('login');
    }

	public function autenticar()
	{
		$this->load->model("usuarios_model");
		$pront = $this->input->post("USR_PRONT");
		$senha = $this->input->post("USR_SENHA");

		$logado = $this->usuarios_model->logar($pront, $senha);

		/*if($status == '2')
		{
			echo 'Usuário bloqueado! Entre em contato com o administrador!';
		}else{*/

		if ($logado)
		{
			redirect('dashboard');
		}else{

			echo 'Usuário ou senha inválidos!';
			//redirect('login');
			
		}
	}
	
	public function logout()
    {
		$this->load->view('logout');
    
	}
	
}
