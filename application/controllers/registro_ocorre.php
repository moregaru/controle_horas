<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro_ocorre extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("usuario");
		if (empty($user))
		{
			redirect('login');
		}
	}
	
	public function index()
	{
		// Recupera os contatos através do model
		$this->load->model('Ocorrencia_Model','model');
		$ocorrencia = $this->model->GetAll('OCOR_NUMCHA');
		// Passa os contatos para o array que será enviado à home
		$dados['ocorrencia'] =$this->model->Formatar($ocorrencia);
		// Chama a home enviando um array de dados a serem exibidos
		$this->load->view('registro_ocorre',$dados);
	}

	public function Excluir(){
		// Recupera o ID do registro - através da URL - a ser editado
		$id = $this->uri->segment(2);
		// Se não foi passado um ID, então redireciona para a home
		if(is_null($id))
			redirect();
		// Remove o registro do banco de dados recuperando o status dessa operação
		$status = $this->contatos_model->Excluir($id);
		// Checa o status da operação gravando a mensagem na seção
		if($status){
			$this->session->set_flashdata('success', '<p>Contato excluído com sucesso.</p>');
		}else{
			$this->session->set_flashdata('error', '<p>Não foi possível excluir o contato.</p>');
		}
		// Redirecionao o usuário para a home
		redirect();
	}
}
