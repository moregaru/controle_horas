<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    function __construct()
	{
		parent::__construct();
        $user = $this->session->userdata("usuario");
        $acess = $this->session->userdata("acesso");
		if (empty($user))
		{
			redirect('login');
		}
		else{
			
			if  ($acess <> "3")
			{
                $this->load->view('usuarios/noacess');
				echo "Usuário sem acesso!";
			}
		}
    }
    
    public function index()
    {
        $this->load->view('usuarios');
    }

    public function novo()
	{
        //$this->load->view('usuarios');
        $USR_PRONT = "";
        $USR_CPF = "";
        $usuario = array(

            "USR_PRONT" => $this->input->post("USR_PRONT"),
            "USR_NOME"  => $this->input->post("USR_NOME"),
            "USR_CPF"   => $this->input->post("USR_CPF"),
            "USR_FONE"  => $this->input->post("USR_FONE"),
            "USR_CEP"   => $this->input->post("USR_CEP"),
            //"USR_UF"    => $this->input->post("USR_UF"),
            "USR_EMAIL" => $this->input->post("USR_EMAIL"),
           // "USR_SENHA" => md5($this->input->post("USR_SENHA")),
           "USR_SENHA" => $this->input->post("USR_SENHA"),
           //"USR_PERFIL"=> $this->input->post("USR_PERFIL"),
            //"USR_PERFIL"=> $this->input->post("USR_STATUS")
        );

        /*if($USR_PRONT == $USR_PRONT ) 
        {
            //ALTERADO LINHA 691 System > Database > db_drive.php
            $this->load->view('usuarios/erro');
        }*/
        
       
        $this->load->model("usuarios_model");
        $this->usuarios_model->salva($usuario);
        $this->load->view('usuarios/novo');
        
	}


}
