<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("usuario");
		$acess = $this->session->userdata("acesso");

		if (empty($user))
		{
			redirect('login');
		}
		
		
	}
	public function index()
	{
		$this->load->view('calendario');
	}
}
