<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reuniao extends CI_Controller {

	/*function __construct()
	{
		parent::__construct();
		$user = $this->session->userdata("usuario");
		if (empty($user))
		{
			redirect('login');
		}
	}*/
	
	public function index()
	{
		// Recupera os contatos através do model
		$this->load->model('Reuniao_Model','model');
		$cursos = $this->model->GetAll('CUR_COD');
		// Passa os contatos para o array que será enviado à home
		$dados['cursos'] =$this->model->Dados($cursos);
		// Chama a home enviando um array de dados a serem exibidos
		$this->load->view('reuniao',$dados);
		
	}
	
}
