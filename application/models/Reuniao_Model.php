<?php
class Reuniao_model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->table = 'cursos';
    }
    /**
    * Formata os contatos para exibição dos dados na home
    *
    * @param array $contatos Lista dos contatos a serem formatados
    *
    * @return array
    */
   function Formatar($cursos){
      if($cursos){
        for($i = 0; $i < count($cursos); $i++){
          //$ocorrencia[$i]['editar_url'] = "index.php/".base_url('editar')."/".$ocorrencia[$i]['ocor_numcha'];
          //$ocorrencia[$i]['excluir_url'] = base_url('excluir')."/".$ocorrencia[$i]['OCOR_NUMCHA'];
        }
        return $cursos;
      } else {
        return false;
      }
    }

    function Dados($cursos)
    {
      $query = $this->db->query("SELECT CUR_COD,CUR_NOME, CUR_COORDENA, CUR_NOMCOORD, CUR_NMDIR
      FROM CURSOS WHERE CUR_NOME <> ''");

      if ($query->num_rows() > 0)
      {
          return $query->result_array();
      } 
      else
      {
        return null;
      }
    }
}