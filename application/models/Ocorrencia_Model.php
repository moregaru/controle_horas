<?php
class Ocorrencia_model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->table = 'ocorrencia';
    }
    /**
    * Formata os contatos para exibição dos dados na home
    *
    * @param array $contatos Lista dos contatos a serem formatados
    *
    * @return array
    */
    function Formatar($ocorrencia){
      if($ocorrencia){
        for($i = 0; $i < count($ocorrencia); $i++){
          //$ocorrencia[$i]['editar_url'] = "index.php/".base_url('editar')."/".$ocorrencia[$i]['ocor_numcha'];
          $ocorrencia[$i]['excluir_url'] = base_url('excluir')."/".$ocorrencia[$i]['OCOR_NUMCHA'];
        }
        return $ocorrencia;
      } else {
        return false;
      }
    }
}