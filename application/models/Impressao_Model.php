<?php
class Impressao_model extends MY_Model {
    function __construct() {
        parent::__construct();
        $this->table = 'materias';
    }
    /**
    * Formata os contatos para exibição dos dados na home
    *
    * @param array $contatos Lista dos contatos a serem formatados
    *
    * @return array
    */
   function Formatar($cursos){
      if($cursos){
        for($i = 0; $i < count($cursos); $i++){
          //$ocorrencia[$i]['editar_url'] = "index.php/".base_url('editar')."/".$ocorrencia[$i]['ocor_numcha'];
          //$ocorrencia[$i]['excluir_url'] = base_url('excluir')."/".$ocorrencia[$i]['OCOR_NUMCHA'];
        }
        return $cursos;
      } else {
        return false;
      }
    }

    function Dados($mat)
    {
      $query = $this->db->query("SELECT CUR_COD, MAT_NMDISCIP, MAT_CODUSR, CUR_NOME, CUR_COORDENA, USR_NOME, CUR_NOMCOORD, CUR_NMDIR
      FROM MATERIAS
      INNER JOIN CURSOS ON MAT_CURSO = CUR_COD
      INNER JOIN USUARIOS ON USR_PRONT = MAT_CODUSR
      WHERE MAT_CURSO <> ''
      AND CUR_COD = '1'");

      if ($query->num_rows() > 1)
      {
          return $query->result_array();
         
      } 
      else
      {
        return null;
      }
    }
}